# Team Effort Log
## Andrei Alexandru Talpan
### Sprint 7 (University Week 11-17) - Christmas Dash
- 1 Task, set and completed in Sprint 7
	- Fix the Analytics Page (2 sub-tasks)
		- Time Spent: 5h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-145)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/403e40ee015827cf641962ea9a18487218371e47)
### Sprint 8 (University Week 18) - Post Christmas Dash
- 1 Task, set and completed in Sprint 8
	- Landing Page 
		- Time Spent: 6h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-171)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/26e0c022f59ee3cfb75cae802ea0e457ac451789)

### Sprint 9 (University Week 19) - Analytics & Admin Improvements
- 1 Task, set and completed in Sprint 9
	- Link Analytics page to Django
		- Time Spent: 1d 4h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-195)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/08150cbdd3af431bf60a90048ed783f81c0ca362)
### Sprint 10  (University Week 20) - Team Report Sprint 1
- 4 Tasks, all set and completed in Sprint 10
	- Charts on the admin analytics page
		- Time Spent: X
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-199)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/a9e40129ae7542d5409ff2db68ae49377fa20a01)
	- Hand Washes bar charts on the analytics page
		- Time Spent: X
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-198)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/500986eeeab5cc63384b051eb42047e9a1caf5f3) 
    - Start writing ProjectMangement.md
	    - Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-208)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/b0ad9fbd87bb6f97535964b6a103a19d7644963e) 
	- TeamEffortLog.md
	    - Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-218)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/792f4a5aeff63e3d32c7f356d45e75c27c1c5e94) 

### Sprint 11 (University Week 21) - Revising documentation
- 3 Tasks, all set and completed in Sprint 11
	- Fix any issues for the analytics page
	    - Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-232)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/05cab968418162a4f98c44644434cb74757e3f9b) 
    - Continue working on ProjectManagement.md
	    - Time Spent: 20 min
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-231)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/af081b3e1324c4e37fb98b2288391c3025a2ff95) 
	- Contribution Slide
		- Time Spent: 5 min
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-242)
### Sprint 12 (University Week 22) - Finishing Touches
- 6 Tasks, 5 set and completed in Sprint 12
	- Design admin/home page 
	    - Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-255)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/fa62f5a476e92e63682001de37a1047c9b32c8c3) 
    - Implement some hover effects on div cards 
	    - Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-256)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/1e8042ef66388cba83553e4a05b2ca7d8f628e76) 
	- TeamEffortLog.md
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-218)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/cb89b15857c8bdcf279dae7fc660be6dfbc506a1)
	- Individual Slides
		- Time Spent: 5 min
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-264)
	- Create individual contribution slide
		- Time Spent: 5 min
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-268)	

## Adam Duarte-Dias
### Sprint 7 (University Week 11-17) - Christmas Dash
- 1 Task, set and completed in Sprint 7
	- Create a Blueprint for the Hardware
		- Time Spent: 5h 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-152)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/blob/31b2664ef8e27010b293f2ea3aba3de909b218b8/documents/blueprint/HardwarePrototyping.md)  
		
### Sprint 8 (University Week 18) - Post Christmas Dash
- 2 Tasks, set and completed in Sprint 8.
	- Attempt to implement Weight Detection into Blueprint
		- Time Spent: 3h 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-174)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/blob/0e479784e8f40bfd205f622b05792f881cc01f0f/documents/blueprint/HardwarePrototyping.md)
	- Implement 3D Model of Sanitization Station
		- Time Spent: 4h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-162)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/blob/0e479784e8f40bfd205f622b05792f881cc01f0f/documents/blueprint/HardwarePrototyping.md)

### Sprint 9  (University Week 19) - Analytics & Admin Improvements
- 1 Task, set and completed in Sprint 9
	- Create a more finalised 3D model for the Case
		- Time Spent - 10h 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-91)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/blob/627066c717ab2446168434a45b246e53ecf6c54b/documents/blueprint/HardwarePrototyping.md)

### Sprint 10  (University Week 20) - Team Report Sprint 1
- 3 Tasks, set and completed in Sprint 9
	- Start writing ProductDemonstration.md 
		- Time Spent - 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-203)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/blob/7ba63ddd366eb5d443ea2e548420d814785d818a/documents/final%20product/ProductDemonstration.md)
	- Write the relevant part (hardware) of the Product Implementation Report
		- Time Spent - 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-221)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/blob/b5d5d5a66e883ef4aad870c06461f8a15296250f/documents/final%20product/ProductImplementationReport.md)
	- TeamEffortLog.md
		- Time Spent - 45m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-216)
		- You're reading it.

### Sprint 11  (University Week 21) - Team Report Sprint 1
- 2 Tasks, 1 set and completed in Sprint 11
	- Perform Manual Testing and Log It
		- Time Spent - 2h 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-229)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/blob/7d77f6985c487244e5970c64b136dd667c3f959b/documents/final%20product/ProductTestingReport.md)
	- Create Individual Contributions/Hardware Slide
		- Not Completed
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-241)
		- Notes: Format changed in sprint report.

### Sprint 12  (University Week 21) - Team Report Sprint 1
- 2 Tasks, set and completed in Sprint 11
	- Individual Slide for Presentation
		- Time Spent - 1h 15m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-259)
		- No gitlab available, Google Slide link [Here.](https://docs.google.com/presentation/d/14G-5l0RYkb4H-KW5UJyBgG8GRHa1G7AqS2kD-xUcTTM/)
	- Proof Read Documentation
		- Time Spent - 3h 10m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-270)
		- Gitlab (PART OF THIS COMMIT, ADD LINK LATER.)

## Knut Sander Lien Blakkestad
### Sprint 7 (University Week 11-17) - Christmas Dash
- 1 Task, set and completed in Sprint 7
	- Gamification: Initial Ideas and Research
		- Time Spent: 3h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-153)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/6c3f0bd2505203d22eb85c9e33c5a39ddd50740d)

### Sprint 8 (University Week 18) - Post Christmas Dash
- 2 Tasks, all set and completed in Sprint 8
	- Update Simulation Script
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-163)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/7cd2c9222f90ffc3ef769e3846c41759546ec5f1)
    - Create Code for Gamification Idea
		- Time Spent: 4h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-157)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/0062839db17754520d5e8aced413725fb475547f)

### Sprint 9 (University Week 19) - Analytics & Admin Improvements
- 2 Tasks, all set and completed in Sprint 9
	- Use the Individual Employees Score
		- Time Spent: 1h 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-184)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/5099a6234f090113fabf74e1fba46b22e0e5fe05)
	- Implement Achievement and Goal Data Idea
		- Time Spent: 3h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-189)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/d0ac38ae3e5ed38304e25a7f9d83d700d46784d9)

### Sprint 10 (University Week 20) - Team Report Sprint 1
- 2 Tasks, all set and completed in Sprint 10
	- Start writing TestReport.md
		- Time Spent: 4h 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-210)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05)
	- Start writing TeamEffortLog.md
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-211)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/26e4d4c30805164d42fea313de8e545414680198)
### Sprint 11 (University Week 21) - Revising documentation
- 3 Tasks, all set and completed in Sprint 21
	- Create TestCases for all views
		- Time Spent: 4h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-224)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/8af386300a5690a97dc13c51347cc431e7f402cc)
	- Continue work on ProductTestingReport.md
		- Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-225)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/a16ccd82e579a2f24226e861ce4b4139633c34f9)
	- Create an individual contribution slide
		- Time Spent: 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-239)
### Sprint 12 (University Week 22) - Finishing Touches
- 4 Tasks and 1 Bug, all set and completed in Sprint 22
	- Write Managing Database part of ProductDemonstration.md
		- Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-257)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/8bb57ed4ba426828433518c03b260598950de8b5)
	- Create test users
		- Time Spent: 10m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-236)
	- Base URL leads to Ubuntu default page
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-247)
	- Create an individual contribution slide
		- Time Spent: 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-239)
	- Individual Slides
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-263)
### Sprint 13 (University Week 23) - Final Product Submission
- 2 Tasks, all set and completed in Sprint 22
	- Film Individual Videos for Presentation
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-293)
	- Finalise and Submit Documentation
		- Time Spent: NaN
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-283)

## Hazim Mohamed-Khan
### Sprint 7 (University Week 11-17) - Christmas Dash
- 1 Story, completed in Sprint 7
	- Create a Login System for Individual Users
		- Time Spent: 6h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-148)
### Sprint 8 (University Week 18) - Post Christmas Dash
- 1 Task, set and completed in Sprint 8
	- Implement the Login System
		- Time Spent: 6h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-158)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/3e63894e7a6bdd58dde9493d1c8809e4f6d1cf19)
### Sprint 9 (University Week 19) - Analytics & Admin Improvements
- 1 Tasks and 2 bugs, all set and 1 task and 1 bug resolved in Sprint 9.
	- Customize dashboard to user
		- Time Spent: 3h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-187)
    - Home link takes you to admin home
		- Time Spent: 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-192)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/59d31f26f96ccf6a73e5f21cef706f76c9d11f95)
### Sprint 10 (University Week 20) - Team Report Sprint 1
 - 1 Task and 1 Sub-Task, set in Sprint 10 and done in Sprint 11.
	- Start writing TeamEffortLog.md
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-217)
        - [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/ff17be938286d76042a4f2c8790610661fab788a)
    - Add Account option in dashboard
		- Time Spent: 3h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-181)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05)
### Sprint 11 (University Week 21) - Revising documentation
- 2 Tasks, set and completed in Sprint 11.
	- Start writing TeamEffortLog.md
		- Time Spent: 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-217)
        - [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/497b060da6aedc9d3d27ec15d235e32a2abbfd3c)
	- Instructions for running our product
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-235)
        - [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/adbd3e942d35df1a24a4224a43553a2f290fa03b)
### Sprint 12 (University Week 22) - Finishing Touches
- 1 Task, 1 Sub-Task and 2 Bugs, all completed except the sub-task completed in sprint 12.
	- Create README.md for final product
		- Time Spent: 3h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-253)
        - [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/37b9a31bc55798ca54254346cf8a60b93477623e)
	- End User logs in to Admin View of Dashboard
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-243)
        - [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/f03d095f0e1637b6b919ccb05bad715dfd830996)
	- Can Access Statistics Dashboard with just URL
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-244)
        - [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/60d4a0798b087da75c6fd82557b016af31600cc9)
### Sprint 13 (University Week 23) - Final Product Submission
- 2 Tasks, 2 Sub-Tasks and 1 bug, all set and completed in sprint 13
	- Create Individual Contribution Slide
		- Time Spent: 
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-294)
	- Create Slides for Webpages
		- Time Spent: 
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-294)
	- Film Individual Videos for Presentation
		- Time Spent: 
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-294)
	- Finalise and Submit Documentation
		- Time Spent: 
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-284)
	- Change Password & Logout only appears on the analytics page
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-274)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/bc1946a75e9f4db016fdfdfd8e06882df5925be7)

## Samuel Engel
### Sprint 7 (University Week 11-17) - Christmas Dash
- 1 Task, all set and completed in Sprint 7
	- Improve and Expand Database Model
		- Time Spent: 3h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-151)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/1155744dcbbbdc918afd5faf1624d8c02eb2c0f0)
	
### Sprint 8 (University Week 18) - Post Christmas Dash
- 3 Tasks, all set and completed in Sprint 8
	- Implement the new database design to the MySQL database on the server
		- Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-160)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/ad15df12ca909ca75aae0019c4fb850ca2a53ca6)
	- Update Django models to work with the new database
		- Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-161)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/eca5d7466490a4be4fd28ce57a7beaeb011fc26e)
### Sprint 9 (University Week 19) - Analytics & Admin Improvements
- 5 Tasks, all set and completed in Sprint 9
	- Generate function to get data for the email report generator
		- Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-177)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/22b865cc7abaf3e3945801687a5d71ac075c3fc3)
	- Extend data generation script to generate data for the new database
		- Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-101)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/157e495603902579e90130a90431591fc0c9dc4e)
### Sprint 10 (University Week 20) - Team Report Sprint 1
- 6 Tasks, all set and completed in Sprint 10
	- Generate users from employees
		- Time Spent: 3h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-207)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/8a05b2bfaf7bc262232aad6d5cafe18f8442c610)
	- Generate random sick days for employees
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-197)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/2391357ceb0d40b6fd66119288282f2bedd28bf9)
	- Start writing ProductContextReport markdown
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-200)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/eb975c7bf58cc650241c36a12c4457dbe6166676)

## Alex Newson
### Sprint 7 (University Week 11-17) - Christmas Dash
- 2 Tasks, all set and completed in Sprint 7
	- Task 1
		- Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-143)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/9fa45347d3628ea73ab654681616ab2cb4ea5b3a)
	- Task 2
		- Time Spent: 45m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-144)
		- GitLab - N/A (Domain)

### Sprint 8 (University Week 18) - Post Christmas Dash
- 5 Tasks, all set and completed in Sprint 8
	- Task 1
		- Time Spent: 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-164)
		- GitLab - N/A (Domain)
	- Task 2
		- Time Spent: 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-165)
		- GitLab - N/A (E-Mail Hosting)
	- Task 3
		- Time Spent: 5m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-166)
		- GitLab - N/A (E-Mail Hosting)
	- Task 4
		- Time Spent: 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-167)
		- GitLab - N/A (Domain)
	- Task 5
		- Time Spent: 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-168)
		- GitLab - N/A (Domain)

### Sprint 9 (University Week 19) - Analytics & Admin
- 3 Tasks, all set and completed in Sprint 9
	- Task 1
		- Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-178)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/867b8e14309c9f846306372d87f3253915ca0e4e)
	- Task 2
		- Time Spent: 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-182)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/74e651df44d5cdd3c5e15c8d4e925f3888d71247) + 3 future commits
	- Task 3
		- Time Spent: 20m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-185)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/a969a09ae8543dfe384b53b1b1f3be770e065fc3)

### Sprint 10 (University Week 20) - Team Report Sprint 1
- 4 Tasks, all set and completed in Sprint 10
	- Task 1
		- Time Spent: 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-202)
		- GitLab - N/A (Web Server)
	- Task 2
		- Time Spent: 15m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-214)
		- GitLab - N/A (Web Server)
	- Task 3
		- Time Spent: 4h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-219)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/b811e5cf614ede317fbc72e988fdc63d52e12bbb)
	- Task 4
		- Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-249)
		- GitLab - N/A (Google Slides)

### Sprint 11 (University Week 21) - Finishing Touches
- 4 Tasks and 2 Bugs all set and completed in Sprint 11
	- Task 1
		- Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-213)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/a334e5e475933188e433e9e12701b79ad592ee85)
	- Task 2
		- Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-238)
		- GitLab - N/A (Google Slides)
	- Task 3
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-250)
		- GitLab - N/A (Web Server)
	- Task 4
		- Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-261)
		- GitLab - N/A (Google Slides)
	- Bug 1
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-196)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/03e87a50c29322b884ce17acc7a00f15f8b43e0b)
	- Bug 2
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-222)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/83fd556febe7944856510361e08ff8a49e7ed94f)
		
## Arif Meighan
### Sprint 7 (University Week 11-17) - Christmas Dash
- 2 Tasks and 1 Bug encountered.
	- Bug 1
		- Time Spend: 1h
		- Fix Clustering Label Method to adjust for different seeds and y values of centroids
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-146)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/731b8a891267f6a47d7b8e5214ef627e61e284ff)
	- Task 1 
		- Time Spend: 5h
		- Research/Design Metrics for the analytical page
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-150)
		- [Gitlab](N/A)
	- Task 2 
		- Time Spend: 6h
		- Create a warning System For high Risk Individuals
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-147)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/8283ab213b7d4d281957f9a047689f85ec28575b)

### Sprint 8 (University Week 18) - Post Christmas Dash
- 2 Tasks
	- Task 1
		- Time Spend: 4h
		- Integrate Email Warning System with existing System
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-159)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/0232adf39db82b79f33a1843f7a580ac65ad647b)
	- Task 2 
		- Time Spend: 5h
		- Research and draw up algorithm for neural network Perceptron
		- Status : Continued to next week
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-173)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/e5027106f6eafc18ab127dbfee274c30c9f0c4a5)

### Sprint 9 (University Week 19) - Analytics & Admin
 - 1 Task with 5 SubTasks
	- Task 1
		- Implement the Neural Network Quarantine prediction algorithm
		- Time Spend: 5h
		- Status : Completed
		- SubTask1 Normalise MA output : done
		- SubTask2 Normalise EMA output : done
		- SubTask3 Implement autoregressive Learning model : Not Completed
		- SubTask4 Obtain Gradient OF sickdays at tangent to specific day : Done
		- SubTask5 Create Simple 4 input Neural Network : Done (was 3 inputs)
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-173)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/44dd993015a58d609d6ed1fa40db3b636e75de0d)
### Sprint 10 (University Week 20) - Team Report Sprint 1
- 3 Tasks and 1 SubTask
	- Task 1
		- Add Algorithms and data structures section to ProductImplementationReport.md
		- Time Spend: 3h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-204)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/5c6fecb8dd6a5ca6457d9fc7734fd23734b95589)
	- Task 2
		- Start writing ProductImplementationReport.md
		- Time Spend: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-206)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/5c6fecb8dd6a5ca6457d9fc7734fd23734b95589)
	- Task 3
		- Storyboard The Presentation Video
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-220)
		- Work was completed in Figma Drafting
		- [Figma](https://www.figma.com/file/7BQ0EIr4uBWtUoj6kvwzRS/Storyboard?node-id=0%3A1)
	- SubTask 1
	    - Draft TeamEffortLog.md Section
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-212)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/bd0e66f8215328548f311da8dc93060bb7ddf2c6)
### Sprint 12 (University Week 22) - Finishing Touches
- 4 Tasks and 1 subtask
	- Task 1
		- Implement Data analytics Slides and script
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-234)
		- Commit was done via google slides
		- [Slides](https://docs.google.com/presentation/d/14G-5l0RYkb4H-KW5UJyBgG8GRHa1G7AqS2kD-xUcTTM/edit?usp=sharing)
	- Task 2
		- Update Project Management with Epic Links and introduction to team working.
		- Time Spent: 3h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-254)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/4b9cb055d5794d544b20ef25840b6b96f54e4383)
	- Task 3
		- Write-Up Project Management Other Areas Section
		- Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-267)
		- [Gitlab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/4b9cb055d5794d544b20ef25840b6b96f54e4383)
	- Task 4
		- Include citations for sources on Project Management and ProductImplementation Report
		- Time Spent: 30m
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-269)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/41665eee1f1a407435bc58302181a7b76ffa7248)
	- SubTask 1
		- Individual Slides
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-260)
### Sprint 13 (University Week 23) - Final Product Submission
- 2 Tasks
	- Task 1
		- Finalise and submit Documentation
		- Time Spent: 2h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-279)
		- [GitLab](https://cseegit.essex.ac.uk/2020_ce299/ce299_team05/-/commit/bd15c6df96efc572328a110662a2046fb8e64d94)
	- Task 2
		- Film Individual Videos for presentation
		- Time Spent: 1h
		- [Jira](https://cseejira.essex.ac.uk/browse/A299109-289)

